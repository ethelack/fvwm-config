This is your ~/.fvwm/notes.txt file annotated onto your desktop wallpaper.

You can reedit it to write any text that you want displayed on your desktop wallpaper.

Quite useful for displaying notes.

Simply open up ~/.fvwm/notes.txt in your favourite text editor and write away.
It is best formatted on screen if you keep the notes about 100 char wide.

Redisplaying the wallpaper runs the ~/.fvwm/scripts/walloftext.sh script which will tail 
the last 43 lines of your notes file.

Use the "Ctrl + Alt + w" key to redisplay the wallpaper with your updated text.

If the wallpaper periodically renews every 5 minutes via a crontab job it will
rerun the walloftext.sh script and redisplay with your updated text.

See line 318 of ~/.fvwm/config on how to enable crontab.

Note that the walloftext.sh script contains a sed command which filters out special
characters and prefixs them with a \ so that the image magick convert command can
annotate to your wallpaper correctly.


