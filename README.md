
My fvwm3 configuration.

To install run 'git clone https://gitlab.com/ethelack/fvwm-config.git ~/.fvwm'

This config was first borrowed from Jay Kuri's config file in 99 and edited since then.

It includes subdirectories of icons, wallpapers and a couple of scripts.

I have made use of a few applications to make it operate like a desktop. Most distros should have these packages available from their repos.

The Applications are:

xdgmenumaker (python-3 dynamic xdg application menu from  https://github.com/gapan/xdgmenumaker)

Stalonetray

notification-daemon

libnotify or dunst for the notify-send command

KDE Network Manager and nm-applet (optional)

rofi

feh or nitrogen

image magick

wbar (optional)

xcompmgr compositor

transset-df  (adjust transparency of windows)

polkit-gnome-authentication-agent-1 (for polkit authentication)

Midnight Commander or ranger   (autostart if installed terminal file manager mc or ranger)

urxvt     (fvwm config starts urxvt terminal by defailt, there are hotkeys for xterm, tilix, alacritty)

firefox    (default web browser)

conky and polybar (optional)

xmessage   (optional for poweroff menu)

flameshot, imlib2 or image magick's import (for screenshots or just use gimp)

pavucontrol, pulsemixer or alsamixer (volume control)

tdrop for scratchpads

curl    (weather report)

Osmo    (taskbar calendar application)

xrandr    (edit xrandr --output options into ~/.profile to set desktop resolution)

There are many comments in the fvwm config file providing the website address from 
which the applications were sourced from. Just do a search for keyword "http" or
run 'grep http ~/.fvwm/config' 

The default startup is 1 Desktop with 6 pages (aka Workspaces) in a row.
4 urxvt terminals including mc will start up on the first 3 workspaces and
firefox starts on the 4th Workspace.


Fvwm Screenshot:

![Fvwm Screenshot](fvwm-screenshot.png "fvwm screenshot")




