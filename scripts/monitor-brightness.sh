#!/bin/bash
# This script requires xrandr and xmessage to adjust and display the current brightness.
# bind the options to your keys for example:

# ~/.config/i3/config
# bindsym $mod+F5 exec monitor-brightness.sh display
# bindsym $mod+F6 exec monitor-brightness.sh minus
# bindsym $mod+F7 exec monitor-brightness.sh normal
# bindsym $mod+F8 exec monitor-brightness.sh plus

# ~/.fluxbox/keys
# Mod4 F5 :Exec monitor-brightness.sh display
# Mod4 F6 :Exec monitor-brightness.sh minus
# Mod4 F7 :Exec monitor-brightness.sh normal
# Mod4 F8 :Exec monitor-brightness.sh plus

# ~/.fvwm/config
# Key F5 A 4 Exec monitor-brightness.sh display
# Key F6 A 4 Exec monitor-brightness.sh minus
# Key F7 A 4 Exec monitor-brightness.sh normal
# Key F8 A 4 Exec monitor-brightness.sh plus



if [ ! -x /usr/bin/xrandr ]; then
    echo "xrandr is not  installed"
fi

if [ ! -x /usr/bin/xmessage ]; then
    echo "xmessage is not  installed"
fi

device=$(xrandr | grep -m1 -w connected | awk -F" " '{ print $1 }')
brightness=$(xrandr --verbose | grep -m1 -iw Brightness | awk -F" " '{ print $2 }')

addbrightness=$(bc <<<"$brightness+0.1")
subtractbrightness=$(bc <<<"$brightness-0.1")


case "$1" in

'current')
echo $brightness
;;

'display')
echo "The current monitor brightness is" $(monitor-brightness.sh current) | xmessage -fg green -bg black -center -file - &
;;

'plus')	
$(xrandr --output $device --brightness $addbrightness)
;;

'minus')
$(xrandr --output $device --brightness $subtractbrightness)
;;

'normal')
$(xrandr --output $device --brightness 1.0)
;;

*)
  echo "usage $0 current|display|plus|minus|normal"
esac

