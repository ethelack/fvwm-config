#!/bin/bash
# display text from ~/.fvwm/notes.txt file on the desktop wallpaper

# in the Convert section random wallpapers are displayed by default from ~/.fvwm/wallpapers

# Comment and uncomment a convert command for your desired wallpaper display

# a sed command rewrites notes.txt as notes.bak suitable for the imagemagick convert command. 
# the convert command redraws the wallpaper with text as wallpaper-text.jpg for feh to display

# Text color is lightgreen by default you can change it with convert option -fill.
# a list of imagemagicks colornames are listed at https://imagemagick.org/script/color.php

# the following TEXT variables are for a string, range of text or tail from notes.bak to display

TEXT1="This is a string"

## sed - display range of lines 300 to 380 of the file
TEXT2=`sed -n '300,380p' ~/.fvwm/notes.bak`

TEXT3=`tail -n 34 ~/.fvwm/notes.bak` 

## sed N is number of tail lines to display options are 38, 43, 48 lines
TEXT4=`sed -e :a -e '$q;N;38,$D;ba' ~/.fvwm/notes.bak`
TEXT5=`sed -e :a -e '$q;N;43,$D;ba' ~/.fvwm/notes.bak`
TEXT6=`sed -e :a -e '$q;N;48,$D;ba' ~/.fvwm/notes.bak`

# sed rewrites ' to literal \' so -draw text can parse ~/.fvwm/notes.bak
cat ~/.fvwm/notes.txt | sed -e "s/'/\\\'/g ; s/\/'/\/\\\'/g ; s/'\b/\\'/g ; s/\b'/\\\'/g ; s/)'/)\\\'/g ; s/(/\\(/g ; s/)/\\)/g" > ~/.fvwm/notes.bak ;

sleep 1

## Convert section 

### uncomment the convert command below for your desired wallpaper setting

## a static greyhills wallpaper
#convert ~/.fvwm/wallpapers/greyhills.jpg  -gravity Northwest -fill lightgreen -font helvetica -pointsize 20 -draw "text 400,80 '$TEXT5'" ~/.fvwm/wallpapers/wallpaper-text.jpg ;


## random wallpaper ~/.fvwm/wallpapers
convert $(shuf -e $(ls ~/.fvwm/wallpapers/*.jpg) -n 1) -gravity Northwest -fill lightgreen -font helvetica -pointsize 20 -draw "text 400,80 '$TEXT5'" ~/.fvwm/wallpapers/wallpaper-text.jpg ;

## random from /usr/share/wallpapers
#convert $(shuf -e $(ls /usr/share/wallpapers/*) -n 1) -gravity Northwest -fill lightgreen -font helvetica -pointsize 20 -draw "text 400,80 '$TEXT5'" ~/.fvwm/wallpapers/wallpaper-text.jpg ;

sleep 2

feh --bg-scale  ~/.fvwm/wallpapers/wallpaper-text.jpg

if [ -x /usr/bin/wbar ]; then
killall wbar 
sleep 1 
wbar --config $HOME/.wbar 
fi

